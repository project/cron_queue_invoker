<?php

namespace Drupal\cron_queue_invoker\Commands;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drush\Style\DrushStyle;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command for checking the status of cron invoked queues.
 */
class CronQueueStatusCommand extends Command {

  /**
   * The queue worker manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueWorkerManager;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Construct the cron invoked queue status command.
   *
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_worker_manager
   *   The queue worker manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(QueueWorkerManagerInterface $queue_worker_manager, QueueFactory $queue_factory, StateInterface $state, TimeInterface $time) {
    parent::__construct();
    $this->queueWorkerManager = $queue_worker_manager;
    $this->queueFactory = $queue_factory;
    $this->state = $state;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('cron-queues:status')
      ->setAliases(['cronq:s'])
      ->setDescription('Get the status of the cron queues.')
      ->addArgument('name', InputArgument::OPTIONAL, 'The worker plugin ID to run. Multiple plugin ids can be comma separated.');
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new DrushStyle($input, $output);

    $plugin_ids = $input->getArgument('name');
    $plugin_ids = $plugin_ids ? explode(',', $plugin_ids) : NULL;

    $headers = [
      'title' => new TranslatableMarkup('Title'),
      'interval' => new TranslatableMarkup('Interval'),
      'status' => new TranslatableMarkup('Status'),
      'last' => new TranslatableMarkup('Last run'),
      'next' => new TranslatableMarkup('Next run'),
      'id' => new TranslatableMarkup('ID'),
    ];

    $intervals = [
      'Y' => new TranslatableMarkup('Yearly'),
      'm' => new TranslatableMarkup('Monthly'),
      'd' => new TranslatableMarkup('Daily'),
      'H' => new TranslatableMarkup('Hourly'),
      'i' => new TranslatableMarkup('Every minute'),
      's' => new TranslatableMarkup('Every second'),
    ];
    $full_format = 'Y-m-d H:i:s';
    $empty_date = '0000-01-01 00:00:00';

    $rows = [];
    foreach ($this->queueWorkerManager->getDefinitions() as $definition) {
      if (!$definition['cron_invoke']) {
        continue;
      }

      if ($plugin_ids && !in_array($definition['id'], $plugin_ids)) {
        continue;
      }

      $queue = $this->queueFactory->get($definition['id']);
      $next_run = NULL;
      $next_run_formatted = '-';
      $format = trim(substr($full_format, 0, strpos($full_format, $definition['cron_invoke']['interval']) + 1), ' :-');
      if ($last_run = $this->state->get($definition['cron_invoke']['state'])) {
        $last_run = DrupalDateTime::createFromTimestamp($last_run);

        $next_run = (clone $last_run);
        switch ($definition['cron_invoke']['interval']) {
          case 'Y':
            $next_run->add(new \DateInterval('P1Y'));
            break;

          case 'm':
            $next_run->add(new \DateInterval('P1M'));
            break;

          case 'd':
            $next_run->add(new \DateInterval('P1D'));
            break;

          case 'H':
            $next_run->add(new \DateInterval('PT1H'));
            break;

          case 'i':
            $next_run->add(new \DateInterval('PT1M'));
            break;

          case 's':
            $next_run->add(new \DateInterval('PT1S'));
            break;

          default:
            $next_run = NULL;
            break;
        }

        if ($next_run) {
          $next_run_formatted = $next_run->format($format);
          $next_run_formatted .= substr($empty_date, strlen($next_run_formatted));
        }
        else {
          $next_run_formatted = '?';
        }
      }

      if ($queue->numberOfItems()) {
        $status = '<info>Active</info>';
      }
      elseif (!$last_run) {
        $status = '<fg=red>Never run</>';
      }
      elseif ($next_run && $next_run->getTimestamp() < $this->time->getRequestTime()) {
        $status = '<fg=red>Due</>';
      }
      else {
        $status = 'Waiting';
      }

      $interval = $intervals[$definition['cron_invoke']['interval']] ??
        new TranslatableMarkup('Invalid [@interval]', [
          '@interval' => $definition['cron_invoke']['interval'],
        ]);

      $rows[] = [
        'title' => $definition['title'],
        'interval' => $interval,
        'status' => $status,
        'last' => $last_run ? $last_run->format($full_format) : '-',
        'next' => $next_run_formatted,
        'id' => $definition['id'],
      ];
    }

    $io->table($headers, $rows);
    $now = DrupalDateTime::createFromTimestamp($this->time->getRequestTime());
    $io->writeln("Generated at {$now->format('Y-m-d H:i:s')}");
  }

}
