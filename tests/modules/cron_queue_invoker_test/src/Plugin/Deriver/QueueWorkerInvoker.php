<?php

namespace Drupal\cron_queue_invoker_test\Plugin\Deriver;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Invoker deriver for tests.
 *
 * @package Drupal\cron_queue_invoker_test\Plugin\Deriver
 */
class QueueWorkerInvoker extends DeriverBase {

  /**
   * Get an array of different test scenarios for queue testing.
   *
   * @return array[]
   *   An array of definitions with different cron_invoke configurations.
   */
  public static function getDefaultDefinitions() {
    return [
      'no_invoker' => [
        'label' => 'Test - No invoker',
        'cron_invoke' => FALSE,
      ],
      'cron_invalid_syntax' => [
        'label' => 'Test - Invalid syntax in cron expression',
        'cron_invoke' => [
          'cron' => '0 7 * * * *',
        ],
      ],
      'cron_invalid_expression' => [
        'label' => 'Test - Invalid cron expression',
        'cron_invoke' => [
          'cron' => '0 7 0 * *',
        ],
      ],
      'interval_day' => [
        'label' => 'Test - Interval day',
        'cron_invoke' => [
          'interval' => 'd',
        ],
      ],
      'interval_month' => [
        'label' => 'Test - Interval month',
        'cron_invoke' => [
          'interval' => 'm',
        ],
      ],
      'cron_daily_morning' => [
        'label' => 'Test - Cron daily morning',
        'cron_invoke' => [
          'cron' => '0 7 * * *',
        ],
      ],
      'cron_daily_morning_night' => [
        'label' => 'Test - Cron daily morning and night',
        'cron_invoke' => [
          'cron' => '30 7,19 * * *',
        ],
      ],
      'cron_weekdays' => [
        'label' => 'Test - Cron daily',
        'cron_invoke' => [
          'cron' => '0 13 * * 1-5',
        ],
      ],
      'cron_weekends' => [
        'label' => 'Test - Cron weekends',
        'cron_invoke' => [
          'cron' => '0 13 * * 6-7',
        ],
      ],
      'cron_monthly' => [
        'label' => 'Test - Cron monthly',
        'cron_invoke' => [
          'cron' => '0 8 3 * *',
        ],
      ],
      'cron_yearly' => [
        'label' => 'Test - Cron yearly',
        'cron_invoke' => [
          'cron' => '37 12 30 3 *',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    if (empty($this->derivatives)) {
      $definitions = static::getDefaultDefinitions();
      foreach ($definitions as $definition_id => $definition) {
        $this->derivatives[$definition_id] = $base_plugin_definition;
        $this->derivatives[$definition_id]['id'] = $base_plugin_definition['id'] . ':' . $definition_id;
        $this->derivatives[$definition_id]['label'] = $definition['label'];
        $this->derivatives[$definition_id]['cron_invoke'] = $definition['cron_invoke'];
      }
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
